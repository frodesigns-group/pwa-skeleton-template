export default {
  siteName: 'PWA Skeleton',
  primaryColor: 'indigo',
  secondaryColor: 'pink',
  colorChoices: [
    'red',
    'pink',
    'purple',
    'deep-purple',
    'indigo',
    'blue',
    'light-blue',
    'cyan',
    'teal',
    'green',
    'light-green',
    'lime',
    'yellow',
    'amber',
    'orange',
    'deep-orange'
  ]
}
