# pwa-skeleton-template

> This template is Vue 2.x compatible.

### Usage

This is a project template for [vue-cli](https://github.com/vuejs/vue-cli).

```bash
$ npm install -g vue-cli
$ vue-init -c gitlab.com:frodesigns-group/pwa-skeleton-template my-project
$ cd my-project
$ npm install
$ npm run dev
```

### What's Included

* `npm run dev`: Webpack + `vue-loader` with proper config for source maps & hot-reload.

* `npm run build`: build with HTML/CSS/JS minification.

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

### Fork It And Make Your Own

You can fork this repo to create your own boilerplate, and use it with `vue-cli`:

```bash
vue init username/repo my-project
```
